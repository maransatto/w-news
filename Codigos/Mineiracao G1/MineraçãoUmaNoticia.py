import pymongo
import requests
from bs4 import BeautifulSoup

req = requests.get('https://g1.globo.com/economia/tecnologia/')
soup = BeautifulSoup(req.content,'html.parser')

linkNoticia = soup.find("a", {"class": "feed-post-link gui-color-primary gui-color-hover"}).get("href")
#print(linkNoticia)

req = requests.get(linkNoticia)
soup = BeautifulSoup(req.content,'html.parser')

categoria = soup.find ("div", {"class": "header-title-content"})
data = soup.find("time", {"itemprop": "datePublished"})
tituloNoticia = soup.find("h1", {"class": "content-head__title"})
subtituloNoticia = soup.find("h2", {"class": "content-head__subtitle"})
textoNoticia = soup.find_all("p", {"class": "content-text__container"})
tam = (len(textoNoticia))
texto = ""

for i in range(tam):
    texto += textoNoticia[i].text
texto = (texto.replace("  ","\n "))

noticia = ("\n "+subtituloNoticia.text+"\n"+texto)

#passar para o mongoDB
conexao = pymongo.MongoClient("mongodb+srv://teste:teste@cluster0-yg3zd.mongodb.net/test?retryWrites=true")
mydb = conexao['Learning']

mydb.noticiaG1.insert(
                            {
                                'categoria':categoria.text,
                                'data':data.text,
                                'titulo':tituloNoticia.text,
                                'noticia': noticia,
                                'link': linkNoticia
                            }
)
	
print('Noticia cadastrada com sucesso!')